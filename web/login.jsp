<%@ include file="nheader.jsp" %>
    <body>
       
        <!-- BEGIN LOGIN -->
     
            <!-- BEGIN LOGIN FORM -->
            <div calss="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            <form class="login-form" action="LoginServlet" method="post">
                <h3 class="form-title font-green"><%= RB.getString("Sign")%> <%= RB.getString("in")%></h3>
              
                  
                   <div class="note note-info">  <center id="error_msg" >
            <%                    if (request.getAttribute("msg") != null) {
                    out.print(String.valueOf(request.getAttribute("msg")));
                }
            %>
        </center>
                </div>
                
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label><%= RB.getString("Email")%></label>
                    <input class="form-control" type="text" autocomplete="off" placeholder="E mail" name="email" /> </div>
                <div class="form-group">
                    <label><%= RB.getString("Password")%> </label>
                    <input class="form-control" type="password" autocomplete="off" placeholder="<%= RB.getString("Password")%>" name="password" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary"><%= RB.getString("Login")%></button>
                    <label class="rememberme check mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /><%= RB.getString("Password")%>
                        <span></span>
                    </label>
                    <a href="forgot.jsp" id="forget-password" class="forget-password"><%= RB.getString("Forgot")%> <%= RB.getString("Password")%>?</a>
                </div>
                <br>
                <div class="create-account">
                    <p>
                        <a href="newregister.jsp" id="register-btn" class="uppercase"><%= RB.getString("Create")%> <%= RB.getString("an")%> <%= RB.getString("account")%></a>
                    </p>
                </div>
                 
            </form>
                </div>
             </div>
        <%@ include file="ifooter.jsp" %>
         </div>
                 </div>
         
        
        
        
      
    </body>

