<%-- 
    Document   : about
    Created on : May 20, 2017, 1:59:07 PM
    Author     : Sakshmapp
--%>


<%@page import="java.sql.PreparedStatement"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 
 
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
      
        <title> All System Transactions</title>

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>
 
        
    
          <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                       <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Blank Page</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Page Layouts</span>
                                </li>
                            </ul>
                            
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title">All System Transactions
                            <small>blank page layout</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        
          <h3 class="form-title font-green">About Us</h3>
          
          
          <div class="infrmtn ng-scope">       
        <div class="cntnt">
        <h3>What is FULLOMM?</h3>
        <p>FULLOMM is available for use at more than 2,50,000 online and offline avenues. Get kwik, hassle-free payments that are super-secure! Use the wallet app to browse the biggest online, retail and fashion stores. Get going now!</p>

        <p>FULLOMM is super-safe. Each and every penny stored in your wallet is well accounted for. You can also use the extra in-app security settings available on all mobile platformsFullOMM is operational on. These are Android, Windows and iOS. All services ofFullOMM are also available via a desktop site and a mobile site.</p>

        <p>BookMyShow, Café Coffee Day, Domino's Pizza, Sagar Ratna, Big Bazaar, Pizza Hut, TastyKhana, JustEat, PVR, eBay, Jabong, Snapdeal, Shopclues, HomeShop18, Naaptol, Pepperfry, Fashionara, FashionAndYou, MakeMyTrip, Ferns N Petals, are just some of the many brands associated withFullOMM.</p>

        <p>FULLOMM received the coveted PrePaid Payment Instrument license from the Reserve Bank of India on 18 July, 2013.</p>

        <h3>Why UseFullOMM: </h3>
        <p><b>Safe &amp; Secure Payments: </b> No need to share your valuable payment details at every other website you shop at. Just useFullOMM to shop from anywhere you want.</p>

        <p><b>Fastest Checkout: </b>With the one-click process, FULLOMM has become the Kwikest checkout option when you are shopping online.</p>
        
        <p><b>Multiple Payments: </b> Store money once and make multiple transactions with FULLOMM.</p>
        <p><b>Discounts &amp; Offers:</b> Get exclusive discounts &amp; cashbacks at partner websites when you pay with your
        FULLOMM wallet.</p>
        </div>
    </div>
          
                
    
      <!-- END CONTENT BODY -->
                </div> </div>
                <!-- END CONTENT -->
              <%@ include file="ifooter.jsp" %>
        
             </body>

</html>