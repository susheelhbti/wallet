<%-- 
    Document   : Wallet
    Created on : Jul 15, 2017, 3:55:09 PM
    Author     : jyotiserver2010
--%>

<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />

        <title> All System Transactions</title>

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>



        <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN THEME PANEL -->
            <!-- END THEME PANEL -->
            <!-- BEGIN PAGE BAR -->
            <div class="page-content-container">
                <div class="page-content-row">

                    <div class="page-content-col">
                        <!-- BEGIN PAGE BASE CONTENT -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light portlet-fit bordered calendar">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-green"></i>
                                            <span class="caption-subject font-green sbold uppercase">All Address</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">



                                            <table id="example"  class="table  table-bordered table-condensed">

                                                <thead>
                                                    <tr>	<th>ID</th>
                                                        <th>RECEIVING ADDRESS</th>

                                                        <th>BALANCE</th>
                                                        <th>TOTAL RECEIVED</th>




                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <%                Connection con = null;
                                                        Statement st = null;
                                                        int row = 0;
                                                        try {
                                                            con = Util.getConnection();
                                                            st = con.createStatement();
                                                            String query = "";

                                                            query = "select count(id) as row from address where username='" + username + "' ";

                                                            System.out.println(query);

                                                            //out.println(query);
                                                            ResultSet rs = st.executeQuery(query);
                                                            String status;

                                                            if (rs.next()) {
                                                                row = rs.getInt("row");
                                                            }

                                                            rs.close();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        } finally {
                                                            try {
                                                                if (st != null) {
                                                                    st.close();
                                                                }
                                                                if (con != null) {
                                                                    con.close();
                                                                }
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }

                                                        String bitid = null;
                                                        String bitaddress = null;
                                                        int insertID = 0;
                                                        int id = 0;
                                                        String password = null;
                                                        String msg = null;
                                                        String e = null;

                                                        con = Util.getConnection();
                                                        st = con.createStatement();
                                                        String query = "select * from register    where    email='" + username + "' ";
                                                        System.out.println(query);
                                                        ResultSet rs = st.executeQuery(query);
                                                        if (rs.next()) {
                                                            password = rs.getString("password");
                                                            id = rs.getInt("id");
                                                            bitid = rs.getString("bitid");

                                                        }
                                                        String u = "";
                                                        // u ="http://127.0.0.1:3000/api/v2/create?password="+ password +"&api_code=321" ;
                                                        u = "http://127.0.0.1:3000/merchant/" + bitid + "/list?password=" + password + "";
                                                        System.out.println(u);

                                                        URL url = new URL(u);
                                                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                                                        conn.setRequestMethod("GET");
                                                        conn.setRequestProperty("Accept", "application/json");
                                                        System.out.println("148");
                                                        if (conn.getResponseCode() != 200) {
                                                            throw new RuntimeException("Failed : HTTP error code : "
                                                                    + conn.getResponseCode());
                                                        }

                                                        BufferedReader br = new BufferedReader(new InputStreamReader(
                                                                (conn.getInputStream())));
                                                        System.out.println("156");
                                                        String output;
                                                        output = br.readLine();
                                                        System.out.println("159");
                                                        // out.println(output);
                                                        System.out.println(output);
                                                        //JSONObject jsonObj = new JSONObject(output);
                                                        System.out.println("152");
                                                        System.out.println(row);
                                        //out.println(row);

                                                        JSONObject root = new JSONObject(output);
                                                        JSONArray sportsArray = root.getJSONArray("addresses");
                                                        // now get the first element:
                                                        int j = 0;
                                                        for (j = 0; j < row; j++) {
                                                            JSONObject firstSport = sportsArray.getJSONObject(j);
                                                            // and so on
                                                            String name = firstSport.getString("address");
                                                            int balance = firstSport.getInt("balance");
                                                            int total = firstSport.getInt("total_received");
                                                            out.print("<tr>");
                                                            out.print("<td>" + j + "</td>");

                                                            out.print("<td>" + name + "</td>");
                                                            out.print("<td>" + balance + "</td>");
                                                            out.print("<td>" + total + "</td>");

                                                            out.print("</tr>");
                                        // basketball
                                                            System.out.println(name);
                                                            System.out.println(balance);
                                                            System.out.println(total);
                }%>
                                                </tbody>	</table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BASE CONTENT -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END
    <!-- END CONTENT -->
    <%@ include file="ifooter.jsp" %>



</html>