<%-- 
    Document   : Wallet
    Created on : Jul 15, 2017, 3:55:09 PM
    Author     : jyotiserver2010
--%>

<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    

        

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>
           <div class="container-fluid">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="sendbitcoin.jsp">Send Bitcoin<span class="sr-only">(current)</span></a></li>
                <li><a href="Receive.jsp">Receive Bitcoin </a></li>
                 <li><a href="TransactionsHistory.jsp">Transactions</a></li>
                 
           </ul>
          


        </div>
                    </div>
                <div class="col-md-6">
            <h2>Send Bitcoins</h2>
            <br>
            <form action="SendBitcoin" method="get">

                        <div class="field-row " id="row_id_address_to">



                            <div id="div_id_address_to" class="label-vertical form-group">

                                <label for="id_address_to" class="control-label requiredField">
                                    Receiving bitcoin address
                                </label>


                                <div class="">
                                    <input autocomplete="off" class="input-xlarge textinput textInput form-control" id="id_address_to" name="address_to" placeholder="Bitcoin address" type="text" />
                                </div>
                            </div>

                        </div>
                        <div class="field-row " id="row_id_amount">

                            <div id="div_id_amount" class="label-vertical form-group">

                                <label for="id_amount" class="control-label requiredField">
                                    Amount in bitcoins
                                </label>
                                <div class="">

                                    <input autocomplete="off" class=" form-control" id="id_amount" name="amount" placeholder="0.0000" type="text" value=""  ng-change="USDBTC()" ng-model="BTC"  ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"/>

                                </div>
                            </div>

                        </div> <div class="field-row " id="row_id_fees">

                            

                        </div>   
                      
                        <div>
                            <h4>More options</h4>
                            <div class="form-group">
                                <div class="label-vertical">
                                    <label>Description</label>
                                </div>
                                <input autocomplete="off" class="form-control" id="id_description" maxlength="100" name="description" placeholder="Appears in the transaction list" type="text" />
                            </div>

                        </div>
                <div class="form-group">
        <div class="label-vertical">
            <label>Amount</label>
        </div>
        <input autocomplete="off" class="form-control amount-field" id="id_fiat_amount" name="fiat_amount" placeholder="0.00" type="text" ng-change="BTCUSD()" ng-model="USD" class=clearfix">
   
    </div>





                  
                        <hr>
                        <!-- end new -->
                        <button id="sendformBtn" type="submit" name="send_submit" class="btn btn-primary" onclick='$(this).hide();$("#loadingText").show();'>
                            <i class="fa fa-btc"></i> Continue
                        </button>

                    
                        
                    </form>
                    </div>
                </div>
            </div>
       
        <%@ include file="ifooter.jsp" %>
            </div>
              </div>
            <script>
    var app = angular.module("myApp", []);
    app.controller("myCtrl", function ($scope, $http) {
        $scope.count = 0;
        $scope.BTCUSD = function () {

            $http.get("currencyconverter").then(function (response) {
                $scope.ab = response.data.usd;
                console.log($scope.ab);



                $scope.BTC = $scope.USD / response.data.usd;
                //$scope.BTC= String.format( "%.2f", $scope.BTC ) ;
            });
        };

        $scope.USDBTC = function () {

            $http.get("currencyconverter").then(function (response) {
                $scope.ab = response.data.usd;
                console.log($scope.ab);

                $scope.USD = $scope.BTC * response.data.usd;
            });

        };



        $http.get("shownotification").then(function (response) {
            $scope.notification = response.data;
            console.log($scope.notification);
        });

        $http.get("GetBalance")
                .then(function (response) {
                    $scope.systemBalance = response.data;
                });
 $http.get("Balance")
    .then(function(response) {
        $scope.walletBalance = response.data;
    });

    });

</script>

</html>