<div class="clearfix abcd"></div>
 <section class="section-fulldark sec-padding">
    <div class="container">
      <div class="row">

<footer class="container">
  <div class="row footer-block">
    <div class="col-md-4 hidden-sm">
      <img id="footer-logo" src="/cached-static/img/site-logo_grey.2c59226a8ab9.png" class="img-responsive"/>
    </div>
    <div class="col-md-2" id="col-about">
      <ul class="nav nav-list">
        <li class="nav-header">ABOUT</li>
        <li><a href="/about">About us</a></li>
        <li><a href="/careers">Careers</a></li>
        <li><a href="/fees">Fees</a></li>
        <li><a href="/whitehat">Security bounties</a></li>
        <li><a href="/statistics">Statistics</a></li>
        <li><a href="/terms_of_service/">Terms of service</a></li>
      </ul>
    </div>
    <div class="col-md-2" id="col-support">
      <ul class="nav nav-list">
        <li class="nav-header">SUPPORT</li>
        <li><a href="/support/request/">Contact support</a></li>
        <li><a href="/faq">FAQ</a></li>
        <li><a href="/guides/">Guides</a></li>
        <li><a href="/password_reset/">Forgot password</a></li>
      </ul>
    </div>
    <div class="col-md-2" id="col-services">
      <ul class="nav nav-list">
        <li class="nav-header">SERVICES</li>
        




<li class="dropdown" id="language-dropdown">

    

    <a class="dropdown-toggle" data-toggle="dropdown" href="#language-dropdown">
        <i class="fa fa-globe"></i>
        English
    </a>

    

    <ul class="dropdown-menu">

        

            

            <li id="lang-select-en">
                <a rel="nofollow" href="/language/set_language_improved?language=en&amp;next=/buy_bitcoins">
                    English (en)
                </a>
            </li>
        

            

            <li id="lang-select-es">
                <a rel="nofollow" href="/language/set_language_improved?language=es&amp;next=/buy_bitcoins">
                    espa�ol (es)
                </a>
            </li>
        

            

            <li id="lang-select-fr">
                <a rel="nofollow" href="/language/set_language_improved?language=fr&amp;next=/buy_bitcoins">
                    fran�ais (fr)
                </a>
            </li>
        

            

            <li id="lang-select-it">
                <a rel="nofollow" href="/language/set_language_improved?language=it&amp;next=/buy_bitcoins">
                    italiano (it)
                </a>
            </li>
        

            

            <li id="lang-select-ru">
                <a rel="nofollow" href="/language/set_language_improved?language=ru&amp;next=/buy_bitcoins">
                    ??????? (ru)
                </a>
            </li>
        

            

            <li id="lang-select-pt-br">
                <a rel="nofollow" href="/language/set_language_improved?language=pt-br&amp;next=/buy_bitcoins">
                    Portugu�s Brasileiro (pt-br)
                </a>
            </li>
        

            

            <li id="lang-select-zh-cn">
                <a rel="nofollow" href="/language/set_language_improved?language=zh-cn&amp;next=/buy_bitcoins">
                    ???? (zh-cn)
                </a>
            </li>
        
    </ul>
</li>

        
        <li><a href="/api-docs/">API documentation</a></li>
        <li><a href="/atm/order-your-own-bitcoin-atm">LocalBitcoins ATM</a></li>
        <li><a href="/affiliate/">Affiliate</a></li>
        <li><a href="https://localbitcoinschain.com">Block Explorer</a></li>
      </ul>
    </div>
    <div class="col-md-2">
          <ul class="nav nav-list">
            <li class="nav-header">FOLLOW US</li>
            <li><a href="https://www.facebook.com/pages/Localbitcoinscom/266849920086274?notif_t=page_new_likes">
              <i class="fa fa-fw fa-facebook"></i>
              &nbsp;Facebook
            </a></li>
            <li><a href="https://twitter.com/LocalBitcoins">
              <i class="fa fa-fw fa-twitter"></i>
              &nbsp;Twitter
            </a></li>
            <li><a href="https://www.instagram.com/localbitcoins/">
              <i class="fa fa-fw fa-instagram"></i>
              &nbsp;Instagram
            </a></li>
            <li><a href="http://www.reddit.com/r/localbitcoins/">
              <i class="fa fa-fw fa-reddit-alien"></i>
              &nbsp;Reddit
            </a></li>
            <li><a href="/irc">
              <i class="fa fa-fw fa-hashtag"></i>
              &nbsp;IRC
            </a></li>
            <li><a href="/blog/">
              <i class="fa fa-fw fa-rss"></i>
              &nbsp;Blog
            </a></li>
            <li><a href="http://www.weibo.com/localbitcoins/">
              <i class="fa fa-fw fa-weibo"></i>
              &nbsp;Chinese Blog
            </a></li>
          </ul>
    </div>
  </div>
</footer>
          </div>
        </div>
     </section>

