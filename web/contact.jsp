<%-- 
    Document   : login
    Created on : May 4, 2017, 2:37:06 PM
    Author     : saksham
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head><style>
            #success_message{ display: none;}</style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



        <%@ include file="nheader.jsp" %>
     <div class="clearfix"></div>

    
    <!-- end header inner -->
    <div class="clearfix"></div>

    
    
    <section class="">
        
    <div class="container content">
        <div class="row">
            <div class="col-lg-12">


          <div class="container">
<h3 class="form-title font-green"> <b>Contact Us </b></h3>

    <form class="well form-horizontal" action=" " method="post"  id="contact_form">
         
<fieldset>

<!-- Form Name -->
<legend>Kindly Fill Your Details</legend>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label">Name</label>  
  <div class="col-md-8 inputGroupContainer">
  <div class="input-group">
 
  <input  name="full_name" placeholder=" Name" class="form-control"  type="text" style="width: 300px;">
    </div>
  </div>
</div>





<!-- Text input-->
       <div class="form-group">
  <label class="col-md-4 control-label">E-Mail</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        
  <input name="email" placeholder="E-Mail Address" class="form-control"  type="text" style="width: 300px;">
    </div>
  </div>
</div>


<!-- Text input-->
       
<div class="form-group">
  <label class="col-md-4 control-label">Phone Number</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
       
  <input name="phone" placeholder="Enter your number" class="form-control" type="number" style="width: 300px;">
    </div>
  </div>
</div>




<!-- radio checks -->
 
<div class="form-group">
  <label class="col-md-4 control-label">Query Description</label>
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        
        	<textarea class="form-control" name="comment" placeholder="Enter your query" style="width: 300px;"></textarea>
  </div>
  </div>
</div>

<!-- Text area -->
  

<!-- Success message -->
<div class="alert alert-success" role="alert" id="success_message"><font color="blue">Success</font> <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-4">
    <button type="submit" class="btn btn-success" >Send <span class="glyphicon glyphicon-send"></span></button>
  </div>
</div>

</fieldset>
</form>
</div>
    </div><!-- /.container -->
    <script src="contact.js" ></script>
            </div>  </div>   </section><!--end Section-->
     <%@ include file="nfooter.jsp" %> 
    