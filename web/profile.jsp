<%-- 
    Document   : Wallet
    Created on : Jul 15, 2017, 3:55:09 PM
    Author     : jyotiserver2010
--%>

<%@page import="AngularAPI.GetBalance"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />

        <title> All System Transactions</title>

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>
    <div class="container-fluid">
        <div class="container">
<div class="row">
    <div class="col-md-6">


          

            <%    Connection con = null;
                Statement st = null;
                String name = null;
                String email = null;

                String phone = null;
                String address = null;

                try {
                    con = Util.getConnection();
                    st = con.createStatement();
                    String query = "";

                    query = "select * from register where username='" + username + "'";

                    System.out.println(query);
                    ResultSet rs = st.executeQuery(query);

                    while (rs.next()) {

                        name = rs.getString("name");
                        email = rs.getString("email");

                        phone = rs.getString("mobile");
                        address = rs.getString("bitaddress");

                    }
                    rs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (st != null) {
                            st.close();
                        }
                        if (con != null) {
                            con.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            %> 



            <h1>Profile</h1>


            <form action="Profile" method="get">
                <div class="form-group ">
                    
                    <label class=" control-label " for="name">
                      <%= RB.getString("Name")%>
                    </label>
                    
                    <input class="form-control" id="call_back_url" name="name" value= "<%= name%>" type="text" placeholder="Call Back URL ">
              
                </div>
                <div class="form-group ">
                    <label class="control-label requiredField" for="email">
                        <%= RB.getString("Email")%>
                        <span class="asteriskField">
                            *
                        </span>
                    </label>
                    <input class="form-control" id="call_back_url" name="email" value= "<%= email%>"  type="text"/>
                </div>

                <div class="form-group ">
                    <label class="control-label requiredField" for="mobile">
                        <%= RB.getString("Mobile")%>
                        <span class="asteriskField">
                            *
                        </span>
                    </label>
                    <input type="text" length="10" pattern="[789][0-9]{9}" class="form-control" id="call_back_url" name="mobile" value= "<%= phone%>"  placeholder="Call Back URL ">

                </div>

                
                <button class="btn btn-primary " name="submit" type="submit">
                   <%= RB.getString("Submit")%>
                </button>
                
                <div class=" ">  <center id="error_msg" >
                        <%                    if (request.getAttribute("msg") != null) {
                                out.print(String.valueOf(request.getAttribute("msg")));

                            }
                        %>
                    </center><h2> <a href="TrackOrder.jsp?username=<%out.print(username);%>"><%= RB.getString("Check")%> <%= RB.getString("ongoing")%> <%= RB.getString("order")%></a> </h2>
                </div>


            </form>
                
    </div>
                <div class="col-md-6">
                 <h1><%= RB.getString("Document")%> <%= RB.getString("for")%> <%= RB.getString("Identification")%> </h1>
                
                
                <form action="fileuploads" method="post" enctype="multipart/form-data">
              <div class="form-group ">
                    <label class="control-label requiredField" for="mobile">
                    <%= RB.getString("Enter")%> <%= RB.getString("Your")%> <%= RB.getString("ID")%> <%= RB.getString("Proof")%> 
                        <span class="asteriskField">
                            *
                        </span>
                    </label>
                       <input type="file" name="file">

              </div>    
                 <button type="submit" class="btn btn-default">Submit</button>
                
                
                </form>    
        </div>
    </div> 
                 
                        <%@ include file="ifooter.jsp" %>
</div>
</div>





    <!-- END CONTENT BODY -->

    <!-- END CONTENT -->


</body>

</html>