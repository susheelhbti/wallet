<%-- 
    Document   : languageconverter
    Created on : Oct 31, 2017, 1:50:14 PM
    Author     : mala singh
--%>

 <%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.Locale"%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.io.* " %> 
  <%
  String lang ="fr"; 
  ResourceBundle RB = ResourceBundle.getBundle("app", new Locale(lang));
%>      
<head>
</head>
<body>
  <p>      
    <%= RB.getString("name") %><br>
    <%= RB.getString("email") %>
    <%= RB.getString("type") %>
    <%= RB.getString("heading") %>
  </p>
</body>
