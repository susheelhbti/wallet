<%-- 
    Document   : register
    Created on : May 4, 2017, 2:32:10 PM
    Author     : saksham
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
         
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Signup...</title>
        
        
         <%@ include file="nheader.jsp" %>
         
  
           <script src="https://www.google.com/recaptcha/api.js"
        async defer>
    </script>

    
    
          <script type="text/javascript">
        function onloadCallback() {
        grecaptcha.render('html_element', {
          'sitekey' : '6Ld1ViYUAAAAALg9Cg9a-0J8uSpUXljgMS1V0whZ'
        });
      };
    </script>

    
    
         
           
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 

<!-- Inline CSS based on choices in "Settings" tab -->
<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: white !important;} .asteriskField{color: red;}</style>
 <div class="page-content-container">
                        <div class="page-content-row">

                            <div class="page-content-col">
                                <!-- BEGIN PAGE BASE CONTENT -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light portlet-fit bordered calendar">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <span class="caption-subject font-green sbold uppercase">Send Bitcoins</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row">
 
         
       
                    <form action="RegisterServlet" class="Register-form" method="post">
  
                        
                           <h3 class="form-title font-green"> <b>Register </b></h3>


                        <div class="form-group"> <label  >Full Name </label>
                            <input class="form-control form-control form-control-solid placeholder-no-fix" type="text" name="fullname" required style="width: 300px;"/></div>



                       <!-- <div class="form-group"> <label  >Username </label>
                            <input class="form-control" type="text" name="username"required /></div>-->


                        <div class="form-group"> <label  >Password </label>
                            <input class="form-control" type="password" name="password" required  style="width: 300px;"/></div>

                        <div class="form-group">	 <label  >Mobile </label>
         <input class="form-control"  pattern="[789][0-9]{9}" type="number" name="mobile" required style="width: 300px;"/></div>


                        <div class="form-group"> <label  >E Mail </label>
                            <input class="form-control" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email" required style="width: 300px;"/></div>


                            
                            
                           <div class="form-group">    <div class="g-recaptcha" data-sitekey="6Ld1ViYUAAAAALg9Cg9a-0J8uSpUXljgMS1V0whZ"></div>
 

                             
                             
                              </div>
                             
                              <div class="checkbox">
                                  <label> 
      <input type="checkbox" > I agree with <a href="privacy.jsp">terms and conditions</a>
    </label>
  </div> 
                             
                             
                        <div class="form-group">
                            <input type="submit" class="btn btn-success register-btn "  name="register" value="Register"  style="width: 300px;"/></div>
                      

<div class="note note-info">  <center id="error_msg" >
            <%                    if (request.getAttribute("msg") != null) {
                    out.print(String.valueOf(request.getAttribute("msg")));
                }
            %>
        </center>
                </div>
                 
                </form>

 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PAGE BASE CONTENT -->
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>

             
                    
                
                
                </div></div></div>

        
      <!--end Section-->
                    
                    <%@ include file="nfooter.jsp" %> 