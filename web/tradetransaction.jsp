<%-- 
    Document   : tradetransaction
    Created on : Jul 19, 2017, 1:03:58 PM
    Author     : jyotiserver2010
--%>


<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->





        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>



        <%@ include file="iheader.jsp" %>

        <!-- BEGIN CONTENT -->
<script>
      
      var   autocomplete;
      

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /* @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

      }
 
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key= AIzaSyAYFWcD0A23YXajXnsBGaAdPe2J41Xu1bM &libraries=places&callback=initAutocomplete"
        async defer></script>
        <!-- BEGIN CONTENT BODY -->

        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN THEME PANEL -->
        <!-- END THEME PANEL -->
        <!-- BEGIN PAGE BAR -->

        <!-- END PAGE BAR -->
     
        <!-- BEGIN PAGE TITLE-->
        <div class="container fluid">
    <div class="container">
        
            <h1 class="page-title">Post A Trade
              
            </h1>
            <!-- END PAGE TITLE-->
            <div ng-app="myApp" ng-controller="myCtrl">



                <form class="form-horizontal" method="post" action="Trade_Tanscation">
                    <div class="form-group">
                        <label class="col-sm-3">I Want Too..</label>
                        <div class="col-sm-9">
                            <label class="radio">
                                <input type="radio"  name="type"  id="Radio1" value="Sell"> Sell bitcoins online
                            </label>
                            <label class="radio">
                                <input type="radio" name="type" id="Radio2" value="Buy"> Buy bitcoins online
                            </label>

                        </div>
                    </div>
                    

                    <div class="form-group" id="locationField">
                        <label class="col-sm-3 control-label">Location</label>
                        
                        
                        <div class="col-sm-6" id="locationField">
                            <input type="text" class="form-control" id="autocomplete" name="location" placeholder="Location">
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="inputlocation" class="col-sm-3 control-label">Payment Method</label>
                        <div class="col-sm-6">
                            <Select class="form-control"  name="payment_method">
                                <option value="paypal">paypal</option>
                                <option value="abc">abc</option>
                                <option value="def">def</option></select>
                        </div>

                    </div>
                    <div class="form-group">

                        <label for="inputlocation" class="col-sm-3 control-label">Currency</label>

                        <div class="col-sm-6">
                            <select class="form-control" name="currency"   >
                                <option ng-repeat="currency in curr" value="{{currency.name}}">
                                    {{currency.name}}
                                </option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">

                        <label for="mar" class="col-sm-3 control-label">Margin</label>

                        <div class="col-sm-6">
                            <input type="number" ng-model="margin"  ng-change="forExConvert()" class="form-control" id="mar" name="margin" placeholder="0" value="0">   
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputnum" class="col-sm-3 control-label">Price Equation</label>
                        <div class="col-sm-6">
                            
                          
                            <input type="text"  ng-model="price_equation" class="form-control" id="inputnum" name="price_equation" placeholder="equation">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputtra" class="col-sm-3 control-label">MIn.Transaction Limit</label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="inputtra" name="min_tranaction" placeholder="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputtra" class="col-sm-3 control-label">Max.Transaction Limit</label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="inputtra"  name="max_tranaction" placeholder="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAmo" class="col-sm-3 control-label">Restricts Amounts To</label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" name="restrict_amount" id="inputAmo" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAmo" class="col-sm-3 control-label">Terms Of Trade</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" name="terms_of_trade" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAmo" class="col-sm-3 control-label">Track Liquidity</label> 

                        <div class="col-sm-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="track_liquidity" value="Yes">

                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputAmo" class="col-sm-3 control-label">identified person only</label> 

                        <div class="col-sm-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="identified_person_only" value="Yes">

                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputAmo" class="col-sm-3 control-label"> SMS verification required </label> 
                        <div class="col-sm-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="sms_verification" value="Yes">

                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAmo" class="col-sm-3 control-label">Trusted People Only</label> 
                        <div class="col-sm-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"  name="trusted_person_only" value="Yes">

                                </label>
                            </div>
                        </div>
                    </div>
                    <br>


                    <input class="btn btn-default " type="submit" value="Publish Advertisement">


                </form>
            </div>
           
            
             <%@ include file="ifooter.jsp" %>
        </div>
    </div>


    <!-- END CONTENT -->
   
    <script>
        var app = angular.module('myApp', []);
        app.controller('myCtrl', function ($scope, $http) {
            $scope.showMe = false;
            


            $http.get("Currency")
                    .then(function (response) {
                        console.log(response);
                        console.log(response.data, name);
                        $scope.curr = response.data;
                    });

$http.get("shownotification").then(function(response){
            $scope.notification = response.data;
            console.log($scope.notification);
            });
 console.log($scope.margin);
  $scope.forExConvert = function() {
            $scope.price_equation = $scope.margin /100;
            $scope.price_equation="btc_in_usd*"+$scope.price_equation;
        };
        });
    </script> 

<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2f5fmMtpn6g071PUZiTyGWHFMhJb1wrM&libraries=places"></script>-->
 
</html>

