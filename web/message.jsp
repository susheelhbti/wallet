
<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />

     

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>

 <%
    if (session.getAttribute("username") != null && !String.valueOf(session.getAttribute("username")).trim().equalsIgnoreCase("")) {


%>      <%@ include file="iheader.jsp" %>
<%} else {

%> 
<%@ include file="nheader.jsp" %>
<%    }
%>

        <!-- BEGIN CONTENT -->
    
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <center>  <span class="caption-subject font-green sbold uppercase">Request submitted... </span></center>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row">
                                    
<div class="col-lg-6 col-sm-offset-3">
    <h2><center id="error_msg" >
            <%
                if (request.getAttribute("msg") != null) {
                    out.print(String.valueOf(request.getAttribute("msg")));
                }
            %>
        </center>
    </h2>
                  
     </div>
                
    
      <!-- END CONTENT BODY -->
                </div>
     
       
                <!-- END CONTENT -->
              <%@ include file="ifooter.jsp" %>
        
             </body>

</html>