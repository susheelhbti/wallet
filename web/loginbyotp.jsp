<%-- 
    Document   : login
    Created on : May 4, 2017, 2:37:06 PM
    Author     : saksham
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



        <%@ include file="nheader.jsp" %>
<div class="clearfix"></div>

    <section>
        <div class="header-inner two">
            <div class="inner text-center">
                <h4 class="title text-white uppercase">papafast</h4>
                <h5 class="text-white uppercase">online recharge</h5>
            </div>
            <div class="overlay bg-opacity-5"></div>
            <img src="inc/b2b-mobile-recharge-software.jpg" alt="" class="img-responsive"/> </div>
    </section>
    <!-- end header inner -->
    <div class="clearfix"></div>
<div class="sakshtitle section-pad bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Login by OTP... </h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container content">
        <div class="row">
            <div class="col-lg-4">



                <form action="OTPLogin" class="login-form" method="post"> 

                    <h3 class="form-title font-green">Login....</h3>
                    <div class="row">
                        <div class="col-sm-12 text-right">



                            <input class="form-control form-control-solid placeholder-no-fix form-group" type="text" autocomplete="off" placeholder="Mobile No" name="mobile" required/> 

                            <button class="btn green" type="submit">Send OTP</button>
                        </div>
                    </div>
                </form><div class="note note-info">  <center id="error_msg" >
            <%                    if (request.getAttribute("msg") != null) {
                    out.print(String.valueOf(request.getAttribute("msg")));
                }
            %>
        </center>
                </div>
            </div>  </div>  </div> 
     <%@ include file="nfooter.jsp" %> 