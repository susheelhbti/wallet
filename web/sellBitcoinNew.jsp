<%-- 
    Document   : tradetransaction
    Created on : Jul 19, 2017, 1:03:58 PM
    Author     : jyotiserver2010
--%>


<%@page import="com.system.currencyconverter"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 


<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <%@ page import="com.login.util.*" %>
    <%@ page import="java.sql.Connection" %>
    <%@ page import="java.sql.Statement" %>
    <%@ page import="java.sql.ResultSet" %>



    <%
        if (session.getAttribute("username") != null && !String.valueOf(session.getAttribute("username")).trim().equalsIgnoreCase("")) {


    %>      <%@ include file="iheader.jsp" %>
    <%} else {

    %> 
    <%@ include file="nheader.jsp" %>
    <%    }


    %>
    <div class="container-fluid">
        <div class="container">


            <div class="row">
                <div class="col-md-12">
                    <%                        String lang = "fr";
                        //String lang ="ch";
                        ResourceBundle RB = ResourceBundle.getBundle("app", new Locale(lang));
                    %> 
                    <h3><%= RB.getString("Sell")%> <%= RB.getString("Bitcoins")%> <%= RB.getString("online")%> <%= RB.getString("in")%> <%= RB.getString("India")%>

                    </h3>
                    <table id="example"  class="table  table-striped table-hover">

                        <thead>
                            <tr>	<th><%= RB.getString("ID")%></th>
                                <th><%= RB.getString("Buyer")%></th>

                                <th><%= RB.getString("Price")%>/BTC </th>
                                <th><%= RB.getString("Limits")%></th>






                                <th><%= RB.getString("Action")%></th>



                            </tr>
                        </thead>

                        <tbody>
                            <%                Connection con = null;
                                Statement st = null;
                                double price = 0.0;
                                double min_transaction = 0.0;
                                double max_transcation = 0.0;
                                currencyconverter c = new currencyconverter();
                                double usd = Double.parseDouble(c.cur());

                                try {
                                    con = Util.getConnection();
                                    st = con.createStatement();
                                    String query = "";
                                    query = "select r.name,t.* from trade_transaction t,register r where t.type='Buy' and t.username = r.username order by t.id desc  ";
                                    // query = "select * from trade_transaction where type='Buy' order by id desc  ";

                                    //  out.println(query);
                                    ResultSet rs = st.executeQuery(query);
                                    String status;
                                    int transaction_id;
                                    while (rs.next()) {
                                        double margin = Double.parseDouble(rs.getString("margin"));
                                        min_transaction = Double.parseDouble(rs.getString("min_transaction"));
                                        max_transcation = Double.parseDouble(rs.getString("max_transcation"));
                                        min_transaction = usd * min_transaction;
                                        max_transcation = usd * max_transcation;
                                        price = usd + (margin * usd / 100);
                                        out.print("<tr>");
                                        out.print("<td>" + rs.getString("id") + "</td>");

                                        out.print("<td>" + rs.getString(1) + "</td>");
                                        out.print("<td>" + price + "</td>");
                                        out.print("<td>" + min_transaction + "$ --" + max_transcation + "$</td>");

                                        // out.print("<td>" + rs.getString("date") + "</td>");
                                        String q1 = "<td> <a href='SellBitCoin.jsp?id=" + rs.getString("id") + "'>Sell</a>";

                                        out.print(q1);

                                        out.print("</tr>");
                                    }
                                    rs.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        if (st != null) {
                                            st.close();
                                        }
                                        if (con != null) {
                                            con.close();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            %>
                        </tbody>	</table>      


                    <%@ include file="ifooter.jsp" %>
                </div>
            </div>
        </div>
    </div>

