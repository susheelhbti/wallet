/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.common;

import com.login.util.Util;
import com.system.Profile;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Saksham
 */
public class addtransaction extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
          try {
            Connection con = null;
            Statement st = null;
            
            
            con = Util.getConnection();
            st = con.createStatement();
            String cs = request.getParameter("cs");
            System.out.println(cs);
            
            
            String name = request.getParameter("name");
            String email = request.getParameter("email");
            String mobile = request.getParameter("mobile");
            HttpSession session = request.getSession();
            String username = String.valueOf(session.getAttribute("username"));
            
            String query = "update register set name='" + name + "'  , mobile='" + mobile + "'    where username='" + username + "'  ";
            System.out.println(query);
            int i = st.executeUpdate(query);
            if (i > 0) {
                request.setAttribute("msg", "Your Details updated Successfully!!");
                RequestDispatcher rq = request.getRequestDispatcher("profile.jsp");
                rq.forward(request, response);
            } else {
                request.setAttribute("msg", "Your Details  are Invalid!!");
                RequestDispatcher rq = request.getRequestDispatcher("profile.jsp");
                rq.forward(request, response);
            }
        } catch (Exception ex) {
            Logger.getLogger(Profile.class.getName()).log(Level.SEVERE, null, ex);
        }
            }

           
 
			
        
        

   public void transactionins(String username,String cr,String dr,String status,String description,String id){
        try {
            Connection con = null;
            Statement st = null;
            
            
            con = Util.getConnection();
            st = con.createStatement();
       String query = "insert into transactions(username,description,cr,dr,trstatus,orderid  ) values ('" + username + "','" + description + "','" + cr + "','" + dr + "','" + status + "','" + id + "')";
                System.out.println(query);
                int i = st.executeUpdate(query);
                if (i > 0) {
            System.out.println(i);
              System.out.println("transaction updated");
                }
          
        } catch (Exception ex) {
            Logger.getLogger(addtransaction.class.getName()).log(Level.SEVERE, null, ex);
        }
   }

      public void transactionupd(String username,String status,String id){
        try {
            Connection con = null;
            Statement st = null;
            
            
            con = Util.getConnection();
            st = con.createStatement();
            PreparedStatement stmt = con.prepareStatement("update into transactions  set trstatus =? where id=? and username=?");
            
     
               stmt.setString(1, status);
                 stmt.setString(2, id);
              
                 stmt.setString(3, username);
            
            
            
            int i3 = stmt.executeUpdate();  
            System.out.println(i3);
            System.out.println("transaction updated");
        } catch (Exception ex) {
            Logger.getLogger(addtransaction.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
