/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.login.util.Util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author jyotiserver2010
 */
public class GetWallet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
        try {
             HttpSession session = request.getSession();
            String username = (String) session.getAttribute("username");
            String address_to = String.valueOf(request.getParameter("address_to"));
            String amount = String.valueOf(request.getParameter("amount"));
            String fees = String.valueOf(request.getParameter("fees"));
            String description = String.valueOf(request.getParameter("description"));
             String bitid=null;
             String bitaddress=null;
             int insertID=0;
             int id=0;
            String password=null;
            String msg=null;
            String e= null;
            Connection con = null;
            Statement st = null;
            con = Util.getConnection();
            st = con.createStatement();
		String query = "select * from register    where    email='" + username + "' ";
            System.out.println(query);
            ResultSet rs = st.executeQuery(query);
            if(rs.next()){
             password=rs.getString("password");
             id=rs.getInt("id");

            }
            String u ="http://127.0.0.1:3000/api/v2/create?password="+ password +"&api_code=321" ;
            System.out.println(u);
            
            URL url = new URL(u);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            System.out.println("148");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            System.out.println("156");
            String output;
            output = br.readLine();
            System.out.println("159");
            System.out.println(output);
            JSONObject jsonObj = new JSONObject(output);
            System.out.println("152");
                      System.out.println(jsonObj.getString("guid"));
                           System.out.println(jsonObj.getString("address"));
PreparedStatement stmtt = con.prepareStatement("update register set bitid=? ,bitaddress=? where id=? ");


                      
                        

                        stmtt.setString(1,  jsonObj.getString("guid"));

                       stmtt.setString(2, jsonObj.getString("address"));

                      stmtt.setString(3,String.valueOf(id));

                       // stmtt.setInt(3, insertID);
                        int i3=0;
System.out.println("update register set bitid='"+jsonObj.getString("guid")+"' ,bitaddress='"+jsonObj.getString("address")+"' where id='"+id+"' ");
//address=jsonObj.getString("address");
try{
                        i3 = stmtt.executeUpdate();
}
catch(Exception a){
    a.getMessage();
    System.out.println(a);
}

                     request.setAttribute("msg", "Address Generated." + jsonObj.getString("address"));

                        RequestDispatcher rq = request.getRequestDispatcher("profile.jsp");

                        rq.forward(request, response);
            
        } catch (JSONException ex) {
          
        } catch (Exception ex) {
           
        }
        }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
