/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.common.addtransaction;
import com.common.notification;
import com.login.util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Saksham
 */
public class Order_status extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        Connection con = null;
        Statement st = null;

        try {
            HttpSession session = request.getSession();
            String username = String.valueOf(session.getAttribute("username")).trim();
            con = Util.getConnection();
            st = con.createStatement();
            String id = request.getParameter("id");
            String casee = request.getParameter("case");
            String host = null;
            String client = "null";
            String paid_by = null;

            if (casee.equals("Confirm")) {

                String query = "";
                query = "select * from bitorder where id='" + id + "'";
                System.out.println(query);
                ResultSet rs1 = st.executeQuery(query);
                while (rs1.next()) {
                    paid_by = rs1.getString("paid_by");
                }
                if (paid_by.equals(username)) {
                    request.setAttribute("msg", "You are not authorized to confirm thr payment!!");
                    RequestDispatcher rq = request.getRequestDispatcher("message.jsp");
                    rq.forward(request, response);
                } else {
                    String q = "select * from bitorder where id='" + id + "'";
                    System.out.println(q);
                    ResultSet rs = st.executeQuery(q);
                    while (rs.next()) {
                        host = rs.getString("host");
                        client = rs.getString("client");
                    }
                    PreparedStatement stmtt = con.prepareStatement("update bitorder set payment_status=?,status=?  where id=? ");

                        stmtt.setString(1, "confirm");
                        stmtt.setString(2, casee);
                 
                    stmtt.setString(3, id);
                    int i3 = 0;
                    System.out.println("update bitorder set payment_status='" + casee + "' status='Paid/" + casee + "'  where id='" + id + "' ");
                    try {
                        request.setAttribute("msg", "Status  updated successfully!!");
                        i3 = stmtt.executeUpdate();

                        
                          addtransaction at=new addtransaction();
                 at.transactionupd(client,"Success",id);
                 at.transactionupd(host,"Success",id);
                 
                 
                 
                        notification no = new notification();
                        no.SaveNotification(host, client, "your order status is  " + casee, "TrackOrder.jsp?username=" + username);
                        email_func en = new email_func();
                        en.SendEmail(host, "Your Order Status is changed .Now,your order status is  " + casee, "FullommTrade");
                        en.SendEmail(client, "Your Order Status is changed .now,your order status is  " + casee, "FullommTrade");
                    } catch (SQLException a) {
                        a.getMessage();
                        System.out.println(a);
                    }
                   // request.setAttribute("msg", "Status  updated successfully!!");
                    RequestDispatcher rq = request.getRequestDispatcher("Feedback.jsp?bitid="+id);
                    rq.forward(request, response);
                }
            } else {
                String q = "select * from bitorder where id='" + id + "'";
                System.out.println(q);
                ResultSet rs = st.executeQuery(q);
                while (rs.next()) {
                    host = rs.getString("host");
                    client = rs.getString("client");
                }
                PreparedStatement stmtt = con.prepareStatement("update bitorder set payment_status=?,status=?  where id=? ");

               if (casee.equals("Dispute")) {
                    stmtt.setString(1, "Dispute");
                    stmtt.setString(2, casee);
                    
                     addtransaction at=new addtransaction();
                 at.transactionupd(client,"Dispute",id);
                 at.transactionupd(host,"Dispute",id);
                 
                } else {
                    stmtt.setString(1, "Pending");
                    stmtt.setString(2, casee);
                    
                     addtransaction at=new addtransaction();
                 at.transactionupd(client,"Pending",id);
                 at.transactionupd(host,"Pending",id);
                }
                stmtt.setString(3, id);
                int i3 = 0;
                System.out.println("update bitorder set payment_status='" + casee + "' status='Paid/" + casee + "'  where id='" + id + "' ");
                try {
                    request.setAttribute("msg", "Status  updated successfully!!");
                    i3 = stmtt.executeUpdate();

                    notification no = new notification();
                    no.SaveNotification(host, client, "your order status is  " + casee, "TrackOrder.jsp?useername=" + username);
                    email_func en = new email_func();
                    en.SendEmail(host, "Your Order Status is changed .Now,your order status is  " + casee, "FullommTrade");
                    en.SendEmail(client, "Your Order Status is changed .now,your order status is  " + casee, "FullommTrade");
                } catch (SQLException a) {
                    a.getMessage();
                    System.out.println(a);
                }
                request.setAttribute("msg", "Status  updated successfully!!");
                RequestDispatcher rq = request.getRequestDispatcher("TrackOrder.jsp?username=" + username);
                rq.forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
