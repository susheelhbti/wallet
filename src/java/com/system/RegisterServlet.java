/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.google.gson.Gson;
import com.login.util.Util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author jyotiserver2010
 */
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        Connection con = null;
        RequestDispatcher rd1;
        Statement st = null;

        try {

            con = Util.getConnection();
            st = con.createStatement();
            String name = String.valueOf(request.getParameter("fullname"));
            String username = String.valueOf(request.getParameter("email"));
            String password = String.valueOf(request.getParameter("password"));
            String mobile = String.valueOf(request.getParameter("mobile"));
            String email = String.valueOf(request.getParameter("email"));
            String ref = String.valueOf(request.getParameter("ref"));
            
            String recap = request.getParameter("g-recaptcha-response");
System.out.println(64);
            // Send get request to Google reCaptcha server with secret key  
           
         URL url = new URL("https://www.google.com/recaptcha/api/siteverify?secret=6LdrUjQUAAAAAM6pqbUuUuWd5rPSYoU3YPIhqHfD&response=" + recap + "&remoteip=" + request.getRemoteAddr());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            String line, outputString = "";
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            while ((line = reader.readLine()) != null) {
                outputString += line;
            }
            System.out.println(outputString);

            CaptchaResponse capRes = new Gson().fromJson(outputString, CaptchaResponse.class);

if (capRes.isSuccess()) {
                String query = "insert into register(name,username,password,mobile,email,status ) values ('" + name + "','" + email + "','" + password + "','" + mobile + "','" + email + "','Verified')";
                System.out.println(query);
                int i = st.executeUpdate(query);
                if (i > 0) {
PreparedStatement stmt = con.prepareStatement("insert into transactions  set trstatus=?, dr=? , cr=? , dr_ac=? , cr_ac=? ,username=?,description=? ");


                        stmt.setString(1, "Success");

                        stmt.setString(2, "0");  //dr 
                        stmt.setString(3,"0");  //cr 
                        stmt.setString(4, username);   //dr ac
                        stmt.setString(5, username);  //cd ac
                        stmt.setString(6, username);  //account of username 
                        stmt.setString(7, "Account setup fee");

                        int i3 = stmt.executeUpdate();
                        
                    /*
                                      
         
                  String msg="<h3>Welcome to papafast! to do login in your account</h3><h3><a href="+"http://papafast.com:8080/papafast/verification.jsp?case=email&email="+email+"&password="+password+""+">Click Here</a></h3><h3>Enjoy!!</h3>";
                	 sms s=new sms();
                        // 
                
                s.sendemail(  msg,  email,   "subject");
                	    msg="<h3>Welcome to papafast! to do login in your account</h3><h3><a href="+"http://papafast.com:8080/papafast/verification.jsp?case=sms&email="+email+"&password="+password+""+">Click Here</a></h3><h3>Enjoy!!</h3>";
                	 s.smssend(msg,mobile);
                                    //session.setAttribute("parent",rs.getString("parent"));
                                 msg="<h3>Welcome to papafast! to do login in your account</h3><h3><a href="+"http://papafast.com:8080/papafast/verification.jsp?case=sms&email="+email+"&password="+password+""+">Click Here</a></h3><h3>Enjoy!!</h3>";
			    	 
                              //  s.smssend(msg,mobile);*/
                     HttpSession session = request.getSession();
                session.setAttribute("username", username);
                session.setAttribute("email", username);
                session.setAttribute("roll", 3);session.setAttribute("mobile", mobile);
                session.setAttribute("sms", "Not");
                   String u = "http://35.192.217.13:8080/Wallet/register?username=" + name + "&password=" + password + "&email=" + email + "&mobile=" + mobile + "";
        System.out.println(u);

        URL url1 = new URL(u);
        System.out.println(url1);
        HttpURLConnection conn1 = (HttpURLConnection) url1.openConnection();
        conn1.setRequestMethod("GET");
        conn1.setRequestProperty("Accept", "application/json");
        System.out.println("148");
        if (conn1.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + conn1.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn1.getInputStream())));
        System.out.println("156");
        String output;
        output = br.readLine();
        System.out.println("159");
        System.out.println(output);

                    request.setAttribute("msg", "Registered successfully....");

                    rd1 = request.getRequestDispatcher("buyBitcoinNew.jsp");
                    rd1.forward(request, response);
                    //  }

                }
}
                  

        } catch (SQLException e) {
            String message = e.getMessage();

            e.printStackTrace();
            request.setAttribute("msg", "Some technical errors in the entered values :'" + message + "'");
            //out.println(message);

            rd1 = request.getRequestDispatcher("message.jsp");
            rd1.forward(request, response);

        } catch (Exception ex) {
            Logger.getLogger(RegisterServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        //out.println(message);
        //RequestDispatcher rd = request.getRequestDispatcher("register.jsp");
        //rd.forward(request, response);

        //RequestDispatcher rd = request.getRequestDispatcher("register.jsp");
        //rd.forward(request, response);
    }

    public void createWallet(String username ) throws JSONException, IOException, SQLException, Exception {
        
        
         
        String email = null;
        String mobile = null;
        int insertID = 0;
        int id = 0;
        String password = null;
        String msg = null;
        String e = null;
        Connection con = null;
        Statement st = null;
        con = Util.getConnection();
        st = con.createStatement();
        String query = "select * from register    where    email='" + username + "' ";
        System.out.println(query);
        ResultSet rs = st.executeQuery(query);
        if (rs.next()) {
            password = rs.getString("password");
            username=rs.getString("name");
            email=rs.getString("email");
            mobile=rs.getString("mobile");
            id = rs.getInt("id");

        }
        String u = "http://35.202.90.95:8080/Wallet/register?username=" + username + "&password=" + password + "&email=" + email + "&mobile=" + mobile + "";
        System.out.println(u);

        URL url = new URL(u);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
        System.out.println("148");
        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + conn.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));
        System.out.println("156");
        String output;
        output = br.readLine();
        System.out.println("159");
        System.out.println(output);
        JSONObject jsonObj = new JSONObject(output);
        System.out.println("152");
       // System.out.println(jsonObj.getString("guid"));
        //System.out.println(jsonObj.getString("address"));
        //PreparedStatement stmtt = con.prepareStatement("update register set bitid=? ,bitaddress=? where id=? ");

        //stmtt.setString(1, jsonObj.getString("guid"));

       // stmtt.setString(2, jsonObj.getString("address"));

        //stmtt.setString(3, String.valueOf(id));

        // stmtt.setInt(3, insertID);
        int i3 = 0;
       // System.out.println("update register set bitid='" + jsonObj.getString("guid") + "' ,bitaddress='" + jsonObj.getString("address") + "' where id='" + id + "' ");
//address=jsonObj.getString("address");
       // try {
        //    i3 = stmtt.executeUpdate();
       // } catch (SQLException a) {
       //     a.getMessage();
       //     System.out.println(a);
      //  }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(RegisterServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(RegisterServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
