/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.login.util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jyotiserver2010
 */
public class ChangePasswordServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       PrintWriter out = response.getWriter();
          Connection con = null;
		Statement st = null;
		
		try {
			con = Util.getConnection();
			st = con.createStatement();
			String cs = request.getParameter("cs");
                        System.out.println(cs);
		  if(cs.equalsIgnoreCase("ChangePassword")) {
			String curPassword = request.getParameter("password");
			String newPassword = request.getParameter("new_password");
			String confPassword = request.getParameter("conf_password");
			
			if(!newPassword.equalsIgnoreCase(confPassword)) {
				request.setAttribute("msg", "Your New Password and Confirm Password are different!!");
			} else {
				HttpSession session=request.getSession();
				String username = String.valueOf(session.getAttribute("username"));
				
				String query = "update register set password='"+newPassword+"' where username='"+username+"' and password='"+curPassword+"'";
				System.out.println(query);
				int i = st.executeUpdate(query);
			    if (i > 0) {
			    	request.setAttribute("msg", "Your Password Change Successfully!!");
                                RequestDispatcher rq = request.getRequestDispatcher("changepassword.jsp");
                    rq.forward(request, response);
			    } else {
			    	request.setAttribute("msg", "Your Current Password is Invalid!!");
                               RequestDispatcher rq = request.getRequestDispatcher("changepassword.jsp");
                    rq.forward(request, response);
			    }
			}
			 }
			 else   
			 { 
				 String call_back_url = request.getParameter("call_back_url");
					String api_key = request.getParameter("api_key");
				 
					 
						HttpSession session=request.getSession();
						String username = String.valueOf(session.getAttribute("username"));
						
						String query = "update register set call_back_url='"+call_back_url+"' , api_key='"+api_key+"' where username='"+username+"'  ";
						System.out.println(query);
						int i = st.executeUpdate(query);
					    if (i > 0) {
					    	request.setAttribute("msg", "Your setings Change Successfully!!");
					    } else {
					    	request.setAttribute("msg", "Your setings is Invalid!!");
					    }
				 
					
			 
			 
			 }
			
			request.setAttribute("module_id", "2");
			RequestDispatcher rq = request.getRequestDispatcher("message.jsp");
			rq.forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(st!=null)
					st.close();
				if(con!=null)
					con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
