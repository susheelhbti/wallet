/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.common.notification;
import com.login.util.Util;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Saksham
 */
public class paymnet_bycllient extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Connection con = null;
        Statement st = null;
        try {
            HttpSession session = request.getSession();
            String username = String.valueOf(session.getAttribute("username"));
            String id = request.getParameter("id");
            String host = null;
            String client = null;
            PrintWriter out = response.getWriter();
            con = Util.getConnection();
            st = con.createStatement();
            System.out.println(12);
            PreparedStatement stmt = con.prepareStatement("update bitorder set status=?,paid_by=?   where id=?");
            stmt.setString(1, "Paid/NotConfirm");
            //stmt.setString(2, parent);
             stmt.setString(2, username);
            stmt.setInt(3, Integer.parseInt(id));
 System.out.println(56);
            int i = stmt.executeUpdate();
 System.out.println(59);
            if (i > 0) {
                 System.out.println(12);
                request.setAttribute("msg", "Submiitted Succesfully!!");
            } else {
                request.setAttribute("msg", "Error in submission,try Again");
            }
            String q = "select * from bitorder where id='" + id + "'";
             System.out.println(q);
            ResultSet rs = st.executeQuery(q);
            while (rs.next()) {
                host = rs.getString("host");
                client = rs.getString("host");
            }
            if (username.equals(host)) { 
                System.out.println(73);
                notification no = new notification();
                no.SaveNotification(username, username, "you " + username + " deposited money to  " + client + ",Please wait for confiramation by " + client, "TrackOrder.jsp?useername=" + username);
                no.SaveNotification(client, client, username + " deposited money to  you" + client + ",PleaseCheck And confirm or dispute", "TrackOrder.jsp?useername=" + client);
                //   no.SaveNotification(client,host,client+" placed order "+host+" to "+type+" and order created successfully.... ","chatbox.jsp?id=" + id);
            } else {
                 System.out.println(78);
                notification no = new notification();
                no.SaveNotification(client, client, "you " + client + " deposited money to  " + username + ",Please wait for confiramation by " + username, "TrackOrder.jsp?useername=" + client);
                no.SaveNotification(username, username, client + " deposited money to you " + username + ",Please Check And confirm or dispute ","TrackOrder.jsp?useername=" + username);
 System.out.println(81);
            }
            RequestDispatcher rq = request.getRequestDispatcher("chatbox.jsp");
            rq.forward(request, response);
        } catch (Exception e) {
            //e.printStackTrace();
        } finally {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
