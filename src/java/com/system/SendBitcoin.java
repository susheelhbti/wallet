/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.system;

import com.login.util.Util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author Saksham
 */
public class SendBitcoin extends HttpServlet {

    private Object session;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");
            Connection con = Util.getConnection();
            Statement st = con.createStatement();
            HttpSession session = request.getSession();
             String username = String.valueOf(session.getAttribute("username")).trim();
             String address_to = String.valueOf(request.getParameter("address_to"));
             String amount = String.valueOf(request.getParameter("amount"));
              String description = String.valueOf(request.getParameter("description"));
                                   
                    String u = "http://35.192.217.13:8080/Wallet/SendBittcoinapi?address_to=" + address_to + "&username=" + username + "&amount=" + amount + "&description=" + description + "";
                            System.out.println(u);

                            URL url = new URL(u);
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setRequestMethod("GET");
                            conn.setRequestProperty("Accept", "application/json");
                            System.out.println("148");
                            if (conn.getResponseCode() != 200) {
                                throw new RuntimeException("Failed : HTTP error code : "
                                        + conn.getResponseCode());
                            }

                            BufferedReader br = new BufferedReader(new InputStreamReader(
                                    (conn.getInputStream())));
                            System.out.println("156");
                            String output;
                            output = br.readLine();
                            System.out.println("159");
                            System.out.println(output);
                            JSONObject jsonObj = new JSONObject(output);
                            System.out.println("152");
                           
                             request.setAttribute("msg", "" + jsonObj.getString("Message"));

              RequestDispatcher rq = request.getRequestDispatcher("message.jsp");
   rq.forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(SendBitcoin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
