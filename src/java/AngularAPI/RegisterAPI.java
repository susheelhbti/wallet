/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AngularAPI;

import com.login.util.Util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author mala singh
 */
public class RegisterAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        //response.setContentType("text/json;charset=UTF-8");
        
         RequestDispatcher rd1;
                     
        Connection con = null;
		Statement st = null;
		  PrintWriter out=response.getWriter();
		try {
                 
			con = Util.getConnection();
			st = con.createStatement();
			String name = String.valueOf(request.getParameter("fullname"));
			String email = String.valueOf(request.getParameter("email"));
			String password = String.valueOf(request.getParameter("password"));
			String mobile =String.valueOf(request.getParameter("mobile"));
			//String email = String.valueOf(request.getParameter("email"));
		 

			// String query = "insert into register(name,email,password,mobile ) values ('" + name + "','" + email + "','" + password + "','" + mobile +")";
	 PreparedStatement stmt = con.prepareStatement("insert into register1(name,email,password,mobile ) values (?,?,?,?)");
		        stmt.setString(1, name);
                        stmt.setString(2, email);  //dr 
                        stmt.setString(3,password);  //cr 
                        stmt.setString(4, mobile);
                      int i = stmt.executeUpdate();
                      //start
                       String encoding = "UTF-8";
            // System.out.print("100");
            String data = "apikey=" + URLEncoder.encode("a515c9ec-0156-42d4-a4aa-244400db4a22", encoding);
            data += "&from=" + URLEncoder.encode("sakshamappinternational@gmail.com", encoding);
            //data += "&from=" + URLEncoder.encode("mridudixit16@gmail.com", encoding);
            data += "&fromName=" + URLEncoder.encode("MobiTop", encoding);
          /*  data += "&subject=" + URLEncoder.encode("Congratulations,Your Regitration is successfull.", encoding);
            data += "&bodyHtml=" + URLEncoder.encode("<h3>Welcome to Mobitop! You can login to your account at https://mobitop.co.in/.</h3>\n" +
"<h3>Here are a couple of tips to help you get started:</h3>\n" +
"<h3>&emsp;For any query,You can contact us at http://mobitop.co.in:8080/MobiTop/index.jsp#contact_section</h3>\n" +

"<h3>Enjoy!!</h3>", encoding);*/
           data += "&subject=" + URLEncoder.encode("Welcome to Mobitop.", encoding);
            data += "&bodyHtml=" + URLEncoder.encode("<h3>Welcome to Mobitop! to do login in your account</h3>\n" +
"<h3><a href="+"http://mobitop.co.in:8080/MobiTop/verification.jsp?email="+email+"&password="+password+""+">Click Here</a></h3>\n" +
"<h3>&emsp;For any query,You can contact us at http://mobitop.co.in:8080/MobiTop/index.jsp#contact_section</h3>\n" +

"<h3>Enjoy!!</h3>", encoding);
            data += "&to=" + URLEncoder.encode(""+email+"", encoding);
            //data += "&to=" + URLEncoder.encode("mridudixit15@gmail.com", encoding);
            data += "&isTransactional=" + URLEncoder.encode("true", encoding);

            URL url = new URL("https://api.elasticemail.com/v2/email/send");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            BufferedReader rd;
            String result;
            try (OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream())) {
                wr.write(data);
                wr.flush();
                rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                result = rd.readLine();
                      
                    
                	 HttpSession session=request.getSession();
			    	session.setAttribute("name",name);
                             session.setAttribute("email",email);
                              session.setAttribute("roll",3);
                               
                                request.setAttribute("msg", "Check your email and login....");
					
                rd.close();
                      //end
                     out.println(i+"you are successfully registered");
                                   
                            }catch (Exception e) {
			
			 out.println("error");
			
		}                 
                }catch (Exception e) {
			
			 out.println("error");
			String message = e.getMessage();
			 	
				e.printStackTrace();
				 request.setAttribute("msg", "Some technical errors in the entered values :'"+message+"'");
		} finally { 
			try {
				if(st!=null)
					st.close();
				if(con!=null)
					con.close();
			} catch (SQLException e) {
			} 
		} 
        
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(RegisterAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(RegisterAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
