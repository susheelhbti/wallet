<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 





<%@ page import="com.login.util.*" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>


<%@ include file="iheader.jsp" %>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="sendbitcoin.jsp"><%= RB.getString("Send")%> Bitcoins<span class="sr-only">(current)</span></a></li>
                            <li><a href="Receive.jsp"><%= RB.getString("Receive")%> Bitcoins </a></li>
                            <!--   <li><a href="transaction.jsp">Transactions</a></li>-->

                            <li><a href="TransactionsHistory.jsp"><%= RB.getString("Transaction")%></a></li>

                        </ul>



                    </div>
                </div>
                <div class="col-md-6">
                    <h2><%= RB.getString("Send")%> Bitcoins</h2>
                    <br>
                    <form action="SendBitcoin" method="get">

                        <div class="field-row " id="row_id_address_to">



                            <div id="div_id_address_to" class="label-vertical form-group">

                                <label for="id_address_to" class="control-label requiredField">
                                    <%= RB.getString("Receiving")%> Bitcoins <%= RB.getString("address")%>
                                </label>


                                <div class="">
                                    <input autocomplete="off" class="input-xlarge textinput textInput form-control" id="id_address_to" name="address_to" placeholder="Bitcoin address" type="text" />
                                </div>
                            </div>

                        </div>
                        <div class="field-row " id="row_id_amount">

                            <div id="div_id_amount" class="label-vertical form-group">

                                <label for="id_amount" class="control-label requiredField">
                                    <%= RB.getString("Amount")%> <%= RB.getString("in")%> Bitcoins
                                </label>
                                <div class="">

                                    <input autocomplete="off" class=" form-control" id="id_amount" name="amount" placeholder="0.0000" type="text" value=""  ng-model="toValue"/>

                                </div>
                            </div>

                        </div> <div class="field-row " id="row_id_fees">



                        </div>   

                        <div>
                            <h4><%= RB.getString("More")%> <%= RB.getString("options")%></h4>
                            <div class="form-group">
                                <div class="label-vertical">
                                    <label><%= RB.getString("Description")%></label>
                                </div>
                                <input autocomplete="off" class="form-control" id="id_description" maxlength="100" name="description" placeholder="Appears in the transaction list" type="text" />
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="label-vertical">
                                <label><%= RB.getString("Amount")%></label>
                            </div>
                            <input autocomplete="off" class="form-control amount-field" id="id_fiat_amount" name="fiat_amount" placeholder="0.00" type="text" data-ng-model="fromValue" data-ng-change="forExConvert()" class=clearfix">
                            <select class="form-control amount-field-fiat" id="id_fiat_currency" name="fiat_currency" ng-model="fromType" class="form-control" required data-ng-change="forExConvert()" ng-options="k for (k, v) in rates track by v">
                                <option value="AED">AED</option>
                                <option value="AFN">AFN</option>
                                <option value="ALL">ALL</option>
                                <option value="AMD">AMD</option>
                                <option value="ANG">ANG</option>
                                <option value="AOA">AOA</option>
                                <option value="ARS">ARS</option>
                                <option value="AUD">AUD</option>
                                <option value="AWG">AWG</option>
                                <option value="AZN">AZN</option>
                                <option value="BAM">BAM</option>
                                <option value="BBD">BBD</option>
                                <option value="BDT">BDT</option>
                                <option value="BGN">BGN</option>
                                <option value="BHD">BHD</option>
                                <option value="BIF">BIF</option>
                                <option value="BTC">BIF</option>
                                <option value="BMD">BMD</option>
                                <option value="BND">BND</option>
                                <option value="BOB">BOB</option>
                                <option value="BRL">BRL</option>
                                <option value="BSD">BSD</option>
                                <option value="BTN">BTN</option>
                                <option value="BWP">BWP</option>
                                <option value="BYN">BYN</option>
                                <option value="BYR">BYR</option>
                                <option value="BZD">BZD</option>
                                <option value="CAD">CAD</option>
                                <option value="CDF">CDF</option>
                                <option value="CHF">CHF</option>
                                <option value="CLF">CLF</option>
                                <option value="CLP">CLP</option>
                                <option value="CNH">CNH</option>
                                <option value="CNY">CNY</option>
                                <option value="COP">COP</option>
                                <option value="CRC">CRC</option>
                                <option value="CUC">CUC</option>
                                <option value="CUP">CUP</option>
                                <option value="CVE">CVE</option>
                                <option value="CZK">CZK</option>
                                <option value="DJF">DJF</option>
                                <option value="DKK">DKK</option>
                                <option value="DOP">DOP</option>
                                <option value="DZD">DZD</option>
                                <option value="EEK">EEK</option>
                                <option value="EGP">EGP</option>
                                <option value="ERN">ERN</option>
                                <option value="ETB">ETB</option>
                                <option value="EUR">EUR</option>
                                <option value="FJD">FJD</option>
                                <option value="FKP">FKP</option>
                                <option value="GBP">GBP</option>
                                <option value="GEL">GEL</option>
                                <option value="GGP">GGP</option>
                                <option value="GHS">GHS</option>
                                <option value="GIP">GIP</option>
                                <option value="GMD">GMD</option>
                                <option value="GNF">GNF</option>
                                <option value="GTQ">GTQ</option>
                                <option value="GYD">GYD</option>
                                <option value="HKD">HKD</option>
                                <option value="HNL">HNL</option>
                                <option value="HRK">HRK</option>
                                <option value="HTG">HTG</option>
                                <option value="HUF">HUF</option>
                                <option value="IDR">IDR</option>
                                <option value="ILS">ILS</option>
                                <option value="IMP">IMP</option>
                                <option value="INR">INR</option>
                                <option value="IQD">IQD</option>
                                <option value="IRR">IRR</option>
                                <option value="ISK">ISK</option>
                                <option value="JEP">JEP</option>
                                <option value="JMD">JMD</option>
                                <option value="JOD">JOD</option>
                                <option value="JPY">JPY</option>
                                <option value="KES">KES</option>
                                <option value="KGS">KGS</option>
                                <option value="KHR">KHR</option>
                                <option value="KMF">KMF</option>
                                <option value="KPW">KPW</option>
                                <option value="KRW">KRW</option>
                                <option value="KWD">KWD</option>
                                <option value="KYD">KYD</option>
                                <option value="KZT">KZT</option>
                                <option value="LAK">LAK</option>
                                <option value="LBP">LBP</option>
                                <option value="LKR">LKR</option>
                                <option value="LRD">LRD</option>
                                <option value="LSL">LSL</option>
                                <option value="LTL">LTL</option>
                                <option value="LVL">LVL</option>
                                <option value="LYD">LYD</option>
                                <option value="MAD">MAD</option>
                                <option value="MDL">MDL</option>
                                <option value="MGA">MGA</option>
                                <option value="MKD">MKD</option>
                                <option value="MMK">MMK</option>
                                <option value="MNT">MNT</option>
                                <option value="MOP">MOP</option>
                                <option value="MRO">MRO</option>
                                <option value="MTL">MTL</option>
                                <option value="MUR">MUR</option>
                                <option value="MVR">MVR</option>
                                <option value="MWK">MWK</option>
                                <option value="MXN">MXN</option>
                                <option value="MYR">MYR</option>
                                <option value="MZN">MZN</option>
                                <option value="NAD">NAD</option>
                                <option value="NGN">NGN</option>
                                <option value="NIO">NIO</option>
                                <option value="NOK">NOK</option>
                                <option value="NPR">NPR</option>
                                <option value="NZD">NZD</option>
                                <option value="OMR">OMR</option>
                                <option value="PAB">PAB</option>
                                <option value="PEN">PEN</option>
                                <option value="PGK">PGK</option>
                                <option value="PHP">PHP</option>
                                <option value="PKR">PKR</option>
                                <option value="PLN">PLN</option>
                                <option value="PYG">PYG</option>
                                <option value="QAR">QAR</option>
                                <option value="RON">RON</option>
                                <option value="RSD">RSD</option>
                                <option value="RUB">RUB</option>
                                <option value="RWF">RWF</option>
                                <option value="SAR">SAR</option>
                                <option value="SBD">SBD</option>
                                <option value="SCR">SCR</option>
                                <option value="SDG">SDG</option>
                                <option value="SEK">SEK</option>
                                <option value="SGD">SGD</option>
                                <option value="SHP">SHP</option>
                                <option value="SLL">SLL</option>
                                <option value="SOS">SOS</option>
                                <option value="SRD">SRD</option>
                                <option value="SSP">SSP</option>
                                <option value="STD">STD</option>
                                <option value="SVC">SVC</option>
                                <option value="SYP">SYP</option>
                                <option value="SZL">SZL</option>
                                <option value="THB">THB</option>
                                <option value="TJS">TJS</option>
                                <option value="TMT">TMT</option>
                                <option value="TND">TND</option>
                                <option value="TOP">TOP</option>
                                <option value="TRY">TRY</option>
                                <option value="TTD">TTD</option>
                                <option value="TWD">TWD</option>
                                <option value="TZS">TZS</option>
                                <option value="UAH">UAH</option>
                                <option value="UGX">UGX</option>
                                <option value="USD">USD</option>
                                <option value="UYU">UYU</option>
                                <option value="UZS">UZS</option>
                                <option value="VEF">VEF</option>
                                <option value="VND">VND</option>
                                <option value="VUV">VUV</option>
                                <option value="WST">WST</option>
                                <option value="XAF">XAF</option>
                                <option value="XAG">XAG</option>
                                <option value="XAR">XAR</option>
                                <option value="XAU">XAU</option>
                                <option value="XCD">XCD</option>
                                <option value="XDR">XDR</option>
                                <option value="XOF">XOF</option>
                                <option value="XPD">XPD</option>
                                <option value="XPF">XPF</option>
                                <option value="XPT">XPT</option>
                                <option value="YER">YER</option>
                                <option value="ZAR">ZAR</option>
                                <option value="ZMK">ZMK</option>
                                <option value="ZMW">ZMW</option>
                                <option value="ZWL">ZWL</option>
                            </select>
                        </div>





<br>
                        <!-- end new -->
                        <button id="sendformBtn" type="submit" name="send_submit" class="btn btn-primary" onclick='$(this).hide();$("#loadingText").show();'>
                            <i class="fa fa-btc"></i> Continue
                        </button>



                    </form>
                </div>
            </div>
        </div>
       
        <%@ include file="ifooter.jsp" %>
    </div>
</div>
<script>
    angular.module('myApp', [])
            .controller('myCtrl', ['$scope', '$http', function ($scope, $http) {
                    $scope.rates = {};
                    $http.get('http://api.fixer.io/latest?base=ZAR')
                            .then(function (res) {
                                $scope.rates = res.data.rates;
                                $scope.toType = $scope.rates.INR;
                                $scope.fromType = $scope.rates.USD;
                                $scope.fromValue = 1;
                                $scope.forExConvert();
                            });
                    $scope.forExConvert = function () {
                        $scope.toValue = $scope.fromValue * ($scope.toType * (1 / $scope.fromType));
                        $scope.toValue = $scope.toValue;
                    };
                    $http.get("shownotification").then(function (response) {
                        $scope.notification = response.data;
                        console.log($scope.notification);
                    });
                    $http.get("GetBalance")
                            .then(function (response) {
                                $scope.systemBalance = response.data;
                            });
                             $http.get("Balance")
    .then(function(response) {
        $scope.walletBalance = response.data;
    });
                }]);
</script>