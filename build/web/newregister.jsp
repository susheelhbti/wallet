<!DOCTYPE html>  
   <html>
  <head>
     <%@ include file="nheader.jsp" %>

<div calss="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
<form class="register-form" action="RegisterServlet" method="post">
                <h3 class="font-green">Sign Up</h3>
                <p class="hint"> Enter your personal details below: </p>
                <div class="form-group">
                    <label>Full Name</label>
                    <input class="form-control" type="text" placeholder="Full Name" name="fullname" /> </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label>Email</label>
                    <input class="form-control" type="text" placeholder="Email" name="email" /> </div>
             
                <div class="form-group">
                    <label>Mobile</label>
                    <input class="form-control" type="text" autocomplete="off" placeholder="Mobile" name="mobile" /> </div>
                <div class="form-group">
                    <label>Password</label>
                    <input class="form-control" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" /> </div>
               <div class="form-group">   
                    
               <div class="g-recaptcha" data-sitekey="6LdrUjQUAAAAAOYSPq_oFs0IhdPWC-sjri_RIhDg"></div>
                     
                </div>
                <div class="form-group margin-top-20 margin-bottom-20">
                    <label class="mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="tnc" /> I agree to the
                        <a href="javascript:;">Terms of Service </a> &
                        <a href="javascript:;">Privacy Policy </a>
                        <span></span>
                    </label>
                    <div id="register_tnc_error"> </div>
                </div>
                <div class="form-actions">
                    <a href="newIndex.jsp">Back</a>
                    <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
                </div>
              
            </form>
                </div>
             </div>
             <%@ include file="ifooter.jsp" %>
         </div>
     </div>
     
        </body>
        </html>