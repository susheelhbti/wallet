  
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.Locale"%>
<%@page import="com.system.logo_Url"%>
<%@page import="com.system.logo_Url"%>

<%@page import="com.common.notification"%>
<%@ page import="java.io.* " %> 

<% int roll = 0;
    if (session.getAttribute("username") != null && !String.valueOf(session.getAttribute("username")).trim().equalsIgnoreCase("")) {
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);

        //
    } else {
        //  roll =1;
%>   <jsp:forward page="login.jsp" />
<%
    }

    String username = String.valueOf(session.getAttribute("username")).trim();
    String rolls = String.valueOf(session.getAttribute("roll")).trim();

    roll = Integer.parseInt(rolls);


%>         



<head>




    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Latest compiled and minified JavaScript -->

    <script
        src="http://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.5/angular.min.js"></script>
    <script type="text/javascript" src='/cached-static/wallet.8d393bd91730.js'></script>


</head>
<!-- END HEAD -->
<body  ng-app="myApp" ng-controller="myCtrl">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <%             String url = request.getRequestURL().toString();
                                logo_Url l = new logo_Url();
                                String a = l.logo(url);

                                System.out.println(a);

                            %>
                            <a class="navbar-brand" href="index.jsp.jsp"><img src="<%=a%>" alt="Logo"/></a>
                        </div>
<%
    String lang = "fr";
    //String lang ="ch";
    ResourceBundle RB = ResourceBundle.getBundle("app", new Locale(lang));
%> 
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="buyBitcoinNew.jsp">
                                        <%= RB.getString("Buy")%> <%= RB.getString("Bitcoins")%> <span class="sr-only">(current)</span></a></li>
                                <li><a href="sellBitcoinNew.jsp"><%= RB.getString("Sell")%> <%= RB.getString("Bitcoins")%></a></li>
                                <li><a href="tradetransaction.jsp"><%= RB.getString("Post")%> <%= RB.getString("A")%> <%= RB.getString("Trade")%></a></li>
                                <li><a href="Forums.jsp"><%= RB.getString("Forums")%></a></li>
                                <li><a href="#"><%= RB.getString("Help")%></a></li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">

                                <li class="dropdown user-panel-dd notifications_dropdown0"  ng-click="show()" data-notification-url="/accounts/notifications/">





                                    <a class="dropdown-toggle notifications-count-base" data-toggle="dropdown"  href="#notifications_dropdown">
                                        <i class="fa fa-comment fa-lg" ></i>
                                        <% notification n = new notification();
                                        String row = n.UnreadNotification(username);%>
                                        <span class="badge read-notification-count" ><%out.print(row);%></span>

                                    </a>

                                    <ul class="dropdown-menu notifications-dropdown scrollable-menu">
                                        <li class="notifications-buttons">
                                            <!--  <ul class="list-inline">
    
                                              <li class="mute-notifications">
                                                  <a class="btn btn-default" href="#"><i class="fa fa-fw fa-volume-off"></i>&nbsp;Mute notifications</a>
                                              </li>
                                              <li class="unmute-notifications display-none">
                                                  <a class="btn btn-default" href="#"><i class="fa fa-fw fa-volume-up"></i>&nbsp;Unmute notifications</a>
                                              </li>
                                          </ul>-->
                                        </li>
                                        <li class="divider"></li>

                                        <li ng-repeat="n in notification" ><a href="{{n.link}}">{{n.notification}}</a></li>

                                    </ul>

                                    <div class="notification-timestamp" style="display: none" data-timestamp="None" data-need-notifications="false"></div></li>
                                <li>

                                    <a id href="newwallet.jsp">

                                        <%= RB.getString("Wallet")%>                    
                                    </a>

                                </li>



                                <li class="dropdown user-panel-dd"> <a class="dropdown-toggle" data-toggle="dropdown" href="#user_dropdown">
                                        <i class="fa fa-user fa-fw"></i><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="profile.jsp" user.username="user.username"><i class="fa fa-user fa-fw"></i>&nbsp; <%=username%></a></li>
                                        <li><a href="profile.jsp"><i class="fa fa-edit fa-fw"></i>&nbsp;<%= RB.getString("Edit")%> <%= RB.getString("profile")%></a></li>
                                        <li><a href="buyBitcoinNew.jsp"><i class="fa fa-dashboard fa-fw"></i>&nbsp;<%= RB.getString("Dashboard")%></a></li>
                                        <li class="divider"></li>
                                        <li class="user-panel-dd"><a href="#"><i class="fa fa-btc fa-fw"></i>&nbsp;<%= RB.getString("Wallet")%>: {{walletBalance}} <%= RB.getString("BTC")%></a></li>
                                        <li class="divider"></li>



                                        <li class="user-panel-dd"><a href="transactions.jsp"><i class="fa fa-shopping-cart fa-fw"></i>&nbsp;<%= RB.getString("Transaction")%> <%= RB.getString("History")%></a>
                                        </li>

                                        <!--
                                        
                                            <li class="two-factor-notification">
                                              <a href="/accounts/security/">Account security:<strong id="dashboard-weak-account-security"> weak </strong></a>
                                            </li>
                                        
                                        
                                        
                                                        <li class="divider"></li>
                                                        <li class="user-panel-dd"><a href="/merchant/"><i class="fa fa-shopping-cart fa-fw"></i>&nbsp;Merchant</a>
                                                        </li>
                                                        <li><a href="/accounts/trusted/"><i class="fa fa-star fa-fw"></i>&nbsp;Trusted</a>
                                                        </li>
                                                        <li class="divider"></li>
                                                        <li class="user-panel-dd"><a href="/support/"><i class="fa fa-ambulance fa-fw"></i>&nbsp;Support</a></li>-->
                                        <li class="divider"></li>
                                        <li><a href="LogoutServlet"><i class="fa fa-sign-out fa-fw"></i>&nbsp;<%= RB.getString("Log")%> <%= RB.getString("out")%></a></li>
                                    </ul>
                                </li>








                                <li class="balance" ><a  > <%= RB.getString("Balance")%>  <%= RB.getString("BTC")%>. {{systemBalance}}</a  ></li> 



                                <!--     <li>
         
                                         <a id="top-login-link" href="profile.jsp">
                                             <i class="fa fa-user">
                                             </i>
                                <%=username%>
                            </a>

                        </li>
                        
                        <li>

                            <a id="top-login-link" href="LogoutServlet">
                                <i class="fa fa-user">
                                </i>
                                Log Out
                            </a>

                        </li>
                                -->


                            </ul>


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </nav>


