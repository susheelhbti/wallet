<!DOCTYPE html>
 
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Imobicoin</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  
      
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->
<%@ include file="iheader.jsp" %>
    <body>
       
        <!-- BEGIN LOGIN -->
     
            <!-- BEGIN LOGIN FORM -->
            <div calss="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            <form class="login-form" action="LoginServlet" method="post">
                <h3 class="form-title font-green">Sign In</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label>E mail</label>
                    <input class="form-control" type="text" autocomplete="off" placeholder="E mail" name="email" /> </div>
                <div class="form-group">
                    <label>Password</label>
                    <input class="form-control" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn green uppercase">Login</button>
                    <label class="rememberme check mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" />Remember
                        <span></span>
                    </label>
                    <a href="forgotpass.jsp" id="forget-password" class="forget-password">Forgot Password?</a>
                </div>
                
                <div class="create-account">
                    <p>
                        <a href="newregister.jsp" id="register-btn" class="uppercase">Create an account</a>
                    </p>
                </div>
            </form>
                </div>
             </div>
         </div>
                 </div>
         
        
        
        
      
    </body>
<%@ include file="ifooter.jsp" %>
</html>