<div class="container-fluid">
                <div class="page-content">
                    <!-- BEGIN BREADCRUMBS -->
                    <div class="breadcrumbs">
                        
                        <ol class="breadcrumb">
                            <li>
                                <a href="profile.jsp">Home</a>
                            </li>
                           
                        </ol>
                        <!-- Sidebar Toggle Button -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-sidebar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="toggle-icon">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </span>
                        </button>
                        <!-- Sidebar Toggle Button -->
                    </div>
                    <!-- END BREADCRUMBS -->
                    <!-- BEGIN SIDEBAR CONTENT LAYOUT -->
                    <div class="page-content-container">
                        <div class="page-content-row">
                            <!-- BEGIN PAGE SIDEBAR -->
                            <div class="page-sidebar">
                                <nav class="navbar" role="navigation">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                   
                                       
                                    <h3>Quick Actions</h3>
                            
                                       <ul class="nav navbar-nav margin-bottom-35">
                                        <li>
                                            <a href="sendbitcoin.jsp">
                                                <i class="icon-envelope "></i> Send Bitcoin
                                               
                                            </a>
                                        </li>
                                        <li>
                                            <a href="Receive.jsp">
                                                <i class="icon-paper-clip "></i> Receive </a>
                                        </li>
                                        <li>
                                            <a href="AllAddress.jsp">
                                                <i class="icon-star"></i> All Address </a>
                                        </li>
                                        
                                    </ul>
                                </nav>
                            </div>
                            <!-- END PAGE SIDEBAR -->