<%-- 
    Document   : TrackOrder
    Created on : Aug 23, 2017, 1:42:36 PM
    Author     : Saksham
--%>


<%@page import="AngularAPI.GetBalance"%>
<%@page import="org.json.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>Track Order</title>

        <%@ page import="com.login.util.*" %>
        <%@ page import="java.sql.Connection" %>
        <%@ page import="java.sql.Statement" %>
        <%@ page import="java.sql.ResultSet" %>
    <%@ include file="iheader.jsp" %><div class="container-fluid">
        <div class="container">
            <div class="row"> 
                <div class="col-md-12">
                    <h1 class="page-title">All Orders....
                    </h1>
                    <div class="note note-info">  <center id="error_msg" >
                            <%                    if (request.getAttribute("msg") != null) {
                                    out.print(String.valueOf(request.getAttribute("msg")));
                                }
                            %>
                        </center>
                    </div>
                    <table id="example"  class="table  table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Head</th>
                                <th>Client</th>

                                <th>Toatal Bitcoin Needes</th> <th>Payment Status</th>
                                <th>Status</th>
                                <th>Chat</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%                Connection con = null;
                                Statement st = null;
                                String us = request.getParameter("username");
                                try {
                                    con = Util.getConnection();
                                    st = con.createStatement();
                                    String query = "";

                                    query = "select b.id,b.client,b.total_bitcoin,b.host,b.payment_status,b.status from bitorder b, trade_transaction t where (b.host='" + us + "' or  b.client='" + us + "') and b.trade_id=t.id ";
        //out.println(query);
                                    ResultSet rs = st.executeQuery(query);
                                    String status;
                                    int transaction_id;
                                    while (rs.next()) {

                                        out.print("<tr>");
                                        out.print("<td>" + rs.getString(1) + "</td>");
                                        out.print("<td> " + rs.getString(4) + "</td>");

                                        out.print("<td> " + rs.getString(2) + "</td>");
                                        out.print("<td> " + rs.getString(3) + "</td>");
                                        out.print("<td> " + rs.getString(5) + "</td>");
                                        out.print("<td> " + rs.getString(6) + "</td>");
                                        if (!rs.getString(6).equals("Suspand")) {

                                            out.print("<td> <A href='chatbox.jsp?id=" + rs.getString(1) + "'>Start Chat</a></td>");
                                        } else {
                                            out.print("<td> </td>");
                                        }
                                        String q1 = "<td> <A href='Order_status?case=Dispute&id=" + rs.getString("id") + "'>Dispute</a>"
                                                + "<br><A href='Order_status?case=Suspand&id=" + rs.getString("id") + "'>Suspand/End Chat</a>"
                                                + "   <br><A href='Order_status?case=Confirm&id=" + rs.getString("id") + "'>Confirm</a> ";
                                        out.print(q1);
                                        out.print("</tr>");
                                    }
                                    rs.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                } finally {
                                    try {
                                        if (st != null) {
                                            st.close();
                                        }
                                        if (con != null) {
                                            con.close();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            %>
                        </tbody> </table></div></div>
                        <%@ include file="ifooter.jsp" %>
            </body>
            </html>