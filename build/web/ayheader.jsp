<%-- 
    Document   : ayheader
    Created on : Jun 30, 2017, 1:02:44 PM
    Author     : jyotiserver2010
--%>

<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#">

<head>
    <link href="http://www.amulyam.in/jsp/images/favicon.ico" rel="shortcut icon" />
    <title>Free Recharge | Free Mobile Recharge | Online Mobile Recharge</title>
    
    
    
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-language" content="en" />
    
    
    
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" type="text/css">
 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
 


    <link rel="stylesheet" type="text/css" media="screen" href="ay/css/all.css" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic|Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/anlcbglalgocacldknganjiiaepfmmnf" />
    <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/lpoekokfaindmdfpacnniglhajimnaca">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <link rel="stylesheet" type="text/css" media="screen" href="ay/css/recharge.css" />
    <script type="text/javascript" src="ay/js/core.js"></script>
    <script type="text/javascript" src="ay/js/common.js" defer="defer"></script>
    <script type="text/javascript" src="ay/js/recharge.js" defer="defer"></script>
    
</head>

<body>
    <div class="inner">
        
        <div class="inner-header">
            
            <div class="amu_new_header">
                <div class="amu_new_main_menu">
                    <div class="main_menu_wrap">
                        <div class="amu_logo">
                            <a href="/"><img src="ay/images/Amulyam-new-logo.png" alt="amulyam logo" /></a>
                        </div>
                        
                        <div class="amu_main_menu">
                            <ul>
                                <li><a class="active_nav remove_dropdown_icon" href="/">Free Recharge</a></li>
                                <li><a href="/shop-and-earn">Shop & Earn <span></span></a>
                                    <ul>
                                        <li><a href="/shop-and-earn">Shop Home</a></li>
                                        <li><a href="/all-categories">All Categories</a></li>
                                        <li><a href="/all-stores">All Stores</a></li>
                                        <li><a href="/banks-and-wallets">All Banks And Wallets</a></li>
                                        <div class="clear"></div>
                                    </ul>
                                </li>
                                <li><a class="remove_dropdown_icon" href="/displayReferFriends.do">Refer & Earn</a></li>
                                
                                
                                 
                            </ul>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
      